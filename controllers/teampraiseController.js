var models = require('../models/index');

// CREATE CONTROLLER
exports.create_post = function(req, res) {
    models.User.create({
        username: req.body.username
    }).then(function() {
        res.redirect('/');
    });
};

// DELETE CONTROLLER
exports.delete_user_get = function(req, res) {
    models.User.destroy({
        where: {
            id: req.params.user_id
        }
    }).then(function() {
        res.redirect('/');
    });
};

// TEAMPRAISE CREATE CONTROLLER
exports.create_teampraise_post = function(req, res) {
    models.Teampraise.create()({
        title: req.body.title,
        UserId: req.params.user_id,
    }).then(function() {
        res.redirect('/');
    });
};

// TEAMPRAISE DELETE CONTROLLER
exports.delete_user_teampraise_get = function(req, res) {
    models.Teampraise.destroy({
        where: {
            id: req.params.teampraise_id
        }
    }).then(function() {
        res.redirect('/');
    });
};
