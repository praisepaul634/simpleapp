var express = require('express');
var router = express.Router();


// Require Controllers
var teampraise_controller = require('../controllers/teampraiseController');

// CREATE ROUTE
router.post('/create', teampraise_controller.create_post);

// DELETE ROUTE
router.post('/:user_id/destroy', teampraise_controller.delete_user_get);

// TEAMPRAISE CREATE
router.post('/:user_id/teampraises/create', teampraise_controller.create_teampraise_post);

// TEAMPRAISE DELETE
router.post('/:user_id/teampraises/:timesheet_id/destroy', teampraise_controller.delete_user_teampraise_get);


module.exports = router;
