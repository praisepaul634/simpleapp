'use strict';

module.exports = (sequelize, DataTypes) => {
    var Teampraise = sequelize.define('Teampraise', {
        name: DataTypes.STRING
    });
    
    Teampraise.associate = function(models) {
        models.Teampraise.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    };
    
    return Teampraise;
};
